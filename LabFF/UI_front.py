'''
@file       UI_front.py
@brief      Collects data from Nucleo to computer
@details    Implements a system to open and run python file in Spyder that acts
            as the UI front end.  In the Spyder console, Pressing 'G' key will
            begin data collection and 'S' key will terminate data collection.
            The data collected is stored in a local array on local and 
            transmitted as a batch to the laptop and saved within a .CSV. Plots
            the data in the PC using the matplotlib module.
            The link to the code can be found here: https://bitbucket.org/mzaman01/me305_labs/src/master/LabFF/UI_front.py
@image html me305lab0xFF1.png "Example When 30 Seconds of Data is Collected " 
@image html me305lab0xFF2.png "Example When Less Than 30 Seconds of Data is Collected " 
@author     Maisha Zaman
@date       February 25th, 2021 
'''

import serial
from matplotlib import pyplot
import keyboard
from array import array 



def key_callback(key):
    """
    @brief callback function called when keyboard key has been pressed
    @param key input to trigger or turn off callback
    """
    
    global last_key
    last_key = key.name
    

#   Main program / test program begin
#   This code only runs if the script is executed as main by pressing play
#   but does not run if the script is imported as a a module

def getData():
    """ 
    @brief Initiates UI_data to start collecting data to send back to UI_front
    @details Sends 'G' to the UI_data class to start running through the data. 
            The data sent back is transferred into a string to be processed.
            Timeout is created if nothing is returned.
    @return Nucleo data in string form
    """
    # pass over key to Nucleo
    ser.write('G'.encode())
    timeout = 0
    while ser.in_waiting == 0:
        timeout += 1
        if timeout > 4000000:
            return None
        elif last_key == "S":
            # print("You've pressed " + last_key)
            ser.write('S'.encode())     # send 'S' to Nucleo
    return ser.readline().decode()
 

if __name__ == "__main__":
 
    ## connect to serial port
    ser = serial.Serial(port='COM3',baudrate=115200,timeout=1)
 
    ## Set state equal to 0
    state = 0 
    
    ## Time array that hold data for 30 secs to plot
    time = array('f', 3001*[])
    
    ## Data array 
    value = array('f', 3001*[]) 
    
    
    # keyboard repsponds to this particular keys
    keyboard.on_release_key("S", callback=key_callback, suppress = True)
    keyboard.on_release_key("G", callback=key_callback, suppress = True)

    ## turns off keyboard callback
    last_key = None

    ## file name to save the data
    # fileName = input("Type the .txt filename you would like to save the data on (ex: UI_data.txt): ")

    ## Create file base to save .CSV data
    fileData = open('UIdata.txt', "w+")
    

    
    print ("""Press uppercase 'G' key to begin data collection for 30 sec. \nWhen data collection starts press uppercase 'S' key if you want to stop.\n""")
    while True:
        try:

            # Wait until G is clicked to initialize data collection
            if last_key is not None:
                # print("You've pressed " + last_key)
                if last_key == "G" or last_key == "S":        
                    ## read values from UART and load into string
                    stringList = getData()
                    
                    # print("stringList: ", stringList)
                    ## Strip String
                    stripList = stringList.strip()
                    # print("stripList: ", stripList)
                    #seperate values into list
                    ## split the stripped string
                    splitList = stripList.split(',')
                    # print("splitList: ", splitList)
                    
                    ## stores numbers, num 1 is time and num 2 is value
                    num1, num2 = float(splitList[0]), float(splitList[1])
                    time.append(float(num1))
                    value.append(float(num2))
                    # print("num1: ", num1)
                    # print("num2: ", num2)
          
                    if num1 != -1:
                        # save string in file and don't include -1 strings
                        fileData.write(stringList) 
          
                    if num1 == -1:        
                        # print("time array:", time)
                        # print("value array:", value)
                        # remove last value of array list, aka the -1s
                        time.pop(-1)
                        value.pop(-1)
                        # Plot points on figure
                        pyplot.figure()
                        pyplot.plot(time,value)
                        pyplot.xlabel('Time (sec)')
                        pyplot.ylabel('Data')
                        print("Thanks!")
                        break
       
            
        except KeyboardInterrupt:
            # This except block catches "Ctrl-C" from the keyboard to end the
            # while(True) loop when desired
            print('Ctrl-c has been pressed')
            break
        
    # close serial port
    ser.close()   
    # turn off callback     
    keyboard.unhook_all()
    #close file
    fileData.close()