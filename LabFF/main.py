'''
@file       main.py
@brief      main file that immediately runs code on nucleo
@details    Runs UI_data class in the background for UI_front to reference to
            The link to the code can be found here: https://bitbucket.org/mzaman01/me305_labs/src/master/LabFF/main.py
@author     Maisha Zaman
@date       February 25th, 2021 
'''



# from pyb import UART

# myuart = UART(2)
# while True:
#     if myuart.any() != 0:
#         val = myuart.readchar()
#         myuart.write('You sent an ASCII '+ str(val) +' to the Nucleo!')
from UI_data import UI_data
# from pyb import UART

if __name__ == "__main__":
    
    # myuart = UART(2)
    # task = UI_data()
    # while True:
    #     try:
    #         if myuart.any() != 0:
    #             key = myuart.readchar()
    #             print("key: ", key)
    #             if key == 71:
    #                task.run(key)
    task = UI_data()
    while True:
        try:
            task.run()
        except KeyboardInterrupt:
            # This except block catches "Ctrl-C" from the keyboard to end the
            # while(True) loop when desired
            print('Ctrl-c has been pressed')
            break
