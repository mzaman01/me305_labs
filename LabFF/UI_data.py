'''
@file       UI_data.py
@brief      A class that collects data on the Nucleo 
@details    Implements a finite state machine to collect data on the Nucleo 
            when 'G' key is clicked. If 'S' is clicked then data collection
            will terminate.The data collected is stored in a local array.
            The link to the code can be found here: https://bitbucket.org/mzaman01/me305_labs/src/master/LabFF/UI_data.py
@image html me305lab0xFF3.png "Finite State Transition Diagram of Nucleo Data Collector" 
@author     Maisha Zaman
@date       February 25th, 2021 
'''



from pyb import UART
from array import array
from math import exp, sin, pi
import utime
import micropython
micropython.alloc_emergency_exception_buf(200)

class UI_data:
    # Static variables are defined outside of methods
    
    ## Initiliaze Program with State 0, waiting for G to be pushed
    S0_WAITING          = 0
    ## State 1 starts collecting and storing data
    S1_COLLECT_DATA     = 1
    ## State 2 sends data to PC
    S2_SEND_DATA        = 2

    

#   Main program / test program begin
#   This code only runs if the script is executed as main by pressing play
#   but does not run if the script is imported as a a module

    def __init__(self):
        
        ## Set state equal to 1
        self.state = 0  
        
        ## Set UART communication       
        self.myuart = UART(2)    
        
        ## Time array that hold data for 30 secs to plot
        self.time = array('f', [0])
        
        ## Data array 
        self.value = array('f', [0]) 
        
        ## iteration of list
        self.idx = 0
    
        self.start = 0   
        
        
    def run(self):
        """
        @brief runs the finite state machine of collecting Data on Nucleo
        """
        # wait until "g" key is clicked to start data collection
        if self.state == self.S0_WAITING:
            # print("State 0 owowowo")
           if self.myuart.any() != 0:
                self.key = self.myuart.read(1).decode()
                # print("key: ", self.key)
                if self.key == "G" or self.key == 'g':
                    # print("g HAS BEEN RECIEVED")
                    ## start recording time
                    self.start = utime.ticks_ms()
                    # print(self.start)
                    #Transition to collecting data on Nucleo
                    self.transitionTo(self.S1_COLLECT_DATA)
                    # print(self.state)
                    self.key = 0
                    # print(self.key)
                       
        # start data collection in Nucleo
        elif self.state == self.S1_COLLECT_DATA:
            # print("state 1")
            ## time updated when state is collecting data
            self.now = utime.ticks_ms()
            
            ## duration of when G is clicked and current time in microsec
            self.duration = utime.ticks_diff(self.now,self.start)
            
            
            # limit values so memory does not exceed
            if self.duration% 100 == 0:
                # print("duration: ", self.duration)
                # add time duration totime  array
                self.time.append(self.duration)     
                ## collect value for each index of time
                self.valueNum = exp(-(self.duration/1000)/10)*sin(2*pi/3*(self.duration/1000))
                # print("valueNum: ", self.valueNum)
                #add value to array
                self.value.append(self.valueNum)
                
                # #increase idx and loop through
                # self.idx += 1
            
            # if 30 seconds is reached, transition to state 3
            if self.duration >= 30000:
                # print("30 seconds of data collected!")
                # self.idx = 0
                self.transitionTo(self.S2_SEND_DATA)
            
            if self.myuart.any() != 0:
                # print("KEY: ", self.key)
                # check to see if S has been clicked and transition to data send
                self.key = self.myuart.read(1).decode()
                # print("KEY: ", self.key)
                if self.key == "S" or self.key == 's':
                    # print("S HAS BEEN RECIEVED")
                    # self.idx = 0
                    self.transitionTo(self.S2_SEND_DATA)
                    self.key = 0
                
            
        # Read and print data collection
        elif self.state == self.S2_SEND_DATA:
            # send data to PC
            out_string = '{:}, {:}\r\n'.format(self.time[self.idx]/1000,self.value[self.idx])
            self.myuart.write(out_string.encode())
            self.idx += 1
            if self.idx == len(self.time):
                stop_string = '{:}, {:}\r\n'.format(-1,-1)
                self.myuart.write(stop_string.encode())
                self.idx = 0
                self.key = 0
                self.transitionTo(self.S0_WAITING)
                
    def transitionTo(self, newState):
        '''
        @brief transitions from current state to next
        @param newState The next desire state
        @return the new state to transition to
        '''
        self.state = newState
        return self.state
            

