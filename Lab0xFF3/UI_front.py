'''
@file       UI_front.py
@brief      Collects data from Nucleo to computer
@details    Implements a system to open and run python file in Spyder that acts
            as the UI front end.  In the Spyder console, the following commands
            perform different actions: 
            Press 'G' encoder 1 data collection for 30 sec.    Press 'F' encoder 2 data collection for 30 sec. \nPress 'S' to end encoder 1 data prematurely.       Press 'L' to end encoder 2 data prematurely.\nPress 'Z' to zero the encoder 1 position.          Press 'A' to zero the encoder 2 position.\nPress 'P' to print out the encoder 1 position      Press 'O' to print out the encoder 2 position.\nPress 'D' to print out encoder 1 delta.            Press 'T' to print out encoder 2 delta.
            The data collected is stored in a local array on local and 
            transmitted as a batch to the laptop and saved within a .CSV. Plots
            the data in the PC using the matplotlib module.
            
            The link to the code can be found here:
@image html taskdiagram1.png "Task Diagram of LabFFx02"
@image html labFFx02graph1.png "Example graph of Encoder 1's position in units of Ticks"
@author     Maisha Zaman
@date       March 18th, 2021 '''

import serial
from matplotlib import pyplot
import keyboard
from array import array 

def key_callback(key):
        """
        @brief callback function called when keyboard key has been pressed
        @param key input to trigger or turn off callback
        """
        
        global last_key
        last_key = key.name
        print("You've pushed the: ", last_key, " key")
    
    #   Main program / test program begin
    #   This code only runs if the script is executed as main by pressing play
    #   but does not run if the script is imported as a a module
    
def getData():
    """ 
    @brief Initiates UI_data to start collecting data to send back to UI_front
    @details Sends 'G' for encoder 1 or 'F' for encoder 2 to the UI_data class 
    to start running through the data. The data sent back is transferred into a string to be        
    processed.Timeout is created if nothing is returned.
    @return Nucleo data in string form
    """
    # pass over key to Nucleo for encoder 1
    if last_key == 'G':
        ser.write('G'.encode())
        timeout = 0
        while ser.in_waiting == 0:
            timeout += 1
            if timeout > 4000000:
                return None
            elif last_key == "S":
                # print("You've pressed " + last_key)
                ser.write('S'.encode())     # send 'S' to Nucleo
        return ser.readline().decode()
    
    # pass over key to Nucleo for encoder 2
    elif last_key == 'F':
        ser.write('F'.encode())
        timeout = 0
        while ser.in_waiting == 0:
            timeout += 1
            if timeout > 4000000:
                return None
            elif last_key == "L":
                # print("You've pressed " + last_key)
                ser.write('L'.encode())     # send 'L' to Nucleo
        return ser.readline().decode()
    
def writeKey(keyLetter):
    """ 
    @brief writes defined key to nucleo end
    @param keyLetter the key to write to nucleo
    @return readlines line from nucleo
    """
    
    ser.write(keyLetter.encode())  
    timeout = 0
    while ser.in_waiting == 0:
        timeout += 1
        if timeout > 4000000:
            return None
    return ser.readline().decode()

if __name__ == "__main__":
    ## connect to serial port
    ser = serial.Serial(port='COM5',baudrate=115200,timeout=1)
    
    ## Time array that hold data for 30 secs to plot
    time = array('f', [])
    
    ## Data array 
    value = array('f', []) 
    
    
    # keyboard repsponds to this particular keys
    keyboard.on_release_key("S", callback=key_callback, suppress = True)
    keyboard.on_release_key("G", callback=key_callback, suppress = True)
    keyboard.on_release_key("Z", callback=key_callback, suppress = True)
    keyboard.on_release_key("P", callback=key_callback, suppress = True)
    keyboard.on_release_key("D", callback=key_callback, suppress = True)   
    keyboard.on_release_key("F", callback=key_callback, suppress = True)
    keyboard.on_release_key("L", callback=key_callback, suppress = True)
    keyboard.on_release_key("A", callback=key_callback, suppress = True)
    keyboard.on_release_key("O", callback=key_callback, suppress = True)
    keyboard.on_release_key("T", callback=key_callback, suppress = True)   
    
    ## turns off keyboard callback
    last_key = None
    
    ## Create file base to save .CSV data
    fileData = open('UIdata.txt', "w+")
     
         
    print ("""Press 'G' encoder 1 data collection for 30 sec.    Press 'F' encoder 2 data collection for 30 sec. \nPress 'S' to end encoder 1 data prematurely.       Press 'L' to end encoder 2 data prematurely.\nPress 'Z' to zero the encoder 1 position.          Press 'A' to zero the encoder 2 position.\nPress 'P' to print out the encoder 1 position      Press 'O' to print out the encoder 2 position.\nPress 'D' to print out encoder 1 delta.            Press 'T' to print out encoder 2 delta.\n """)
        
      
    while True:
        try:

            # Wait until G is clicked to initialize data collection
            if last_key is not None:
                # print("You've pressed " + last_key)
                
                if last_key == "Z" or last_key == "A":
                    #zero the encoders
                    print(writeKey(last_key))
                    last_key = None
                
                elif last_key == "P" or last_key == "O":
                    #print the encoder position
                    print(writeKey(last_key))
                    last_key = None
                    
                elif last_key == "D" or last_key == "T":
                    #print the encoder delta
                    print(writeKey(last_key))
                    last_key = None
                    
                elif last_key == "G" or last_key == 'F' or last_key == "S" or last_key == 'L':        
                    ## read values from UART and load into string
                    stringList = getData()
                    
                    # print("stringList: ", stringList)
                    ## Strip String
                    stripList = stringList.strip()
                    # print("stripList: ", stripList)
                    
                    #seperate values into list
                    ## split the stripped string
                    splitList = stripList.split(',')
                    # print("splitList: ", splitList)
                    
                    ## stores numbers, num 1 is time and num 2 is value
                    num1, num2 = float(splitList[0]), float(splitList[1])
                    time.append(float(num1))
                    value.append(float(num2))
                    # print("num1: ", num1)
                    # print("num2: ", num2)
          
                    if num1 != -1:
                        # save string in file and don't include -1 strings
                        fileData.write(stringList) 
          
                    if num1 == -1:        
                        # print("time array:", time)
                        # print("value array:", value)
                        # remove last value of array list, aka the -1s
                        time.pop(-1)
                        value.pop(-1)
                        # Plot points on figure
                        pyplot.figure()
                        pyplot.plot(time,value)
                        pyplot.xlabel('Time (sec)')
                        pyplot.ylabel('Data')
                        print("Data Collection is complete, Thanks!")
                        break
       
            
        except KeyboardInterrupt:
            # This except block catches "Ctrl-C" from the keyboard to end the
            # while(True) loop when desired
            print('Ctrl-c has been pressed')
            break
        
    # close serial port
    ser.close()   
    # turn off callback     
    keyboard.unhook_all()
    #close file
    fileData.close()