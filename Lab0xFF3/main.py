'''
@file       main.py
@brief      Runs the controller class and UI class on the Nucleo
@details    This code contains the controller class and UI class that is ran
            on the nucleo upon start. It runs in the background waiting for
            the PC end to start initiating the UI task class. 
            The link to the code can be found here: 
@image html taskdiagram1.png "Task Diagram of LabFFx02"
@author     Maisha Zaman
@date       March 18th, 2021
'''

from controllerTsk import controllerTask
from UI_data import uiTask
import shares


if __name__ == "__main__":
    # Program initialization goes here

    task1 = controllerTask(250000)
    task2 = uiTask()
    
    while True:
        try:
   
            task1.run()
            task2.run()
          
            
        except KeyboardInterrupt:
            # This except block catches "Ctrl-C" from the keyboard to end the
            # while(True) loop when desired
            print('Ctrl-c has been pressed')
            break
