'''
@file       UI_data.py
@brief      Class that collects data on the Nucleo and sends to PC front end
@details    Implements a finite state machine to collect data on the Nucleo 
            when 'G' or 'F' key is clicked. The data collected is stored in a local array.
            and sent back to the frontend. If 'S' or 'L' is clicked then data collection
            will terminate. If 'P' or 'O' is clicked the position is printed.
            If 'Z' or 'A' is printed, the position is zeroed out. If 'D' or 'T' 
            is clicked, the delta is printed out. 
            The link to the code can be found here: 
@image html me305lab0xFF3.png "Finite State Transition Diagram of Nucleo Data Collector" 
@author     Maisha Zaman
@date       March 18th, 2021
'''

import shares
from pyb import UART
from array import array
import utime
import micropython
micropython.alloc_emergency_exception_buf(200)


class uiTask:
    # Static variables are defined outside of methods
    
    ## Initiliaze Program with State 0, waiting for G to be pushed
    S0_WAITING          = 0
    ## State 1 starts collecting and storing data
    S1_COLLECT_DATA     = 1
    ## State 2 sends data to PC
    S2_SEND_DATA        = 2
    
    
    def __init__(self):
        """
        @brief collects encoder driver counter data and stores into an array
        """         
        ## Set state equal to 0
        self.state = 0  
        
         ## Set UART communication       
        self.myuart = UART(2)    
        
        ## Time array that hold data for 30 secs to plot
        self.time = array('f', [0])
        
        ## Data array 
        self.value = array('f', [0]) 
        
        ## iteration of list
        self.idx = 0
        
        
    def run(self):
        """
        @brief runs the finite state machine of collecting Data on Nucleo
        """
        # wait until "g" key is clicked to start data collection
        if self.state == self.S0_WAITING:
           # print("State 0 owowowo")
          
           if self.myuart.any() != 0:
                self.key = self.myuart.read(1).decode()
                # print("key: ", self.key)
                
                #start data collection
                if self.key == "G" or self.key == 'F':
                    # print("g HAS BEEN RECIEVED")
                    ## start recording time
                    self.start = utime.ticks_ms()
                    # print(self.start)
                    #Transition to collecting data on Nucleo
                    self.transitionTo(self.S1_COLLECT_DATA)
                    
                    ## set mode to figure which encoder to collect data for
                    self.mode = self.key
                    
                    # print(self.state)
                    self.key = 0
                
                # zero out position
                elif self.key == "Z":
                    self.myuart.write("Encoder 1's position has been set to zero".encode())
                    shares.setZero1 = True
                    self.key = 0
                    
                elif self.key == "A":
                    self.myuart.write("Encoder 2's position has been set to zero".encode())
                    shares.setZero2 = True
                    self.key = 0
                    
                # recieve position of encoders
                elif self.key == "P":
                    # position string of encoder 1
                    posStr1 = "Encoder 1's position is: {:}".format(shares.enc1Pos)
                    self.myuart.write(posStr1.encode())
                    self.key = 0
                    
                elif self.key == "O":
                    # position string of encoder 2
                    posStr2 ="Encoder 2's position is: {:}".format(shares.enc2Pos)
                    self.myuart.write(posStr2.encode())
                    self.key = 0
                    
                #recieve delta of encoders
                elif self.key == "D":
                    delStr1 = "Encoder 1's delta is: {:} ".format(shares.enc1Del)
                    self.myuart.write(delStr1.encode())
                    self.key = 0
                elif self.key == "T":
                    delStr2 = "Encoder 2's delta is: {:} ".format(shares.enc2Del)
                    self.myuart.write(delStr2.encode())
                    self.key = 0
                  
                       
        # start data collection in Nucleo
        elif self.state == self.S1_COLLECT_DATA:
            # print("state 1")
            ## time updated when state is collecting data
            self.now = utime.ticks_ms()
            
            ## duration of when G is clicked and current time in microsec
            self.duration = utime.ticks_diff(self.now,self.start)
            
            
            # limit values so memory does not exceed
            if self.duration% 100 == 0:
                # print("duration: ", self.duration)
                
                # add time duration totime  array
                self.time.append(self.duration) 
                
                # encoder 1 data collection
                if self.mode == "G":
                
                    ## collect encoder updated value for each index of time
                    # self.valueNum = self.encoder1.get_position()
                    # print("valueNum: ", self.valueNum)
                    
                    #add value to array
                    self.value.append(shares.enc1Pos)
                    
                #encoder 2 data collection
                elif self.mode == "F":
                    
                    ## collect encoder updated value for each index of time
                    # self.valueNum = self.encoder2.get_position()
                    # print("valueNum: ", self.valueNum)
                    
                    #add value to array
                    self.value.append(shares.enc2Pos)
                    
                else:
                    pass
            
            # if 30 seconds is reached, transition to state 3
            if self.duration >= 30000:
                # print("30 seconds of data collected!")
                # self.idx = 0
                self.transitionTo(self.S2_SEND_DATA)
            
            if self.myuart.any() != 0:
                # print("KEY: ", self.key)
                # check to see if S has been clicked and transition to data send
                self.key = self.myuart.read(1).decode()
                # print("KEY: ", self.key)
                if self.key == "S" or self.key == 'L':
                    # print("S HAS BEEN RECIEVED")
                    # self.idx = 0
                    self.transitionTo(self.S2_SEND_DATA)
                    self.key = 0
                
            
        # Read and print data collection
        elif self.state == self.S2_SEND_DATA:
            # send data to PC
            out_string = '{:}, {:}\r\n'.format(self.time[self.idx]/1000,self.value[self.idx])
            self.myuart.write(out_string.encode())
            self.idx += 1
            if self.idx == len(self.time):
                stop_string = '{:}, {:}\r\n'.format(-1,-1)
                self.myuart.write(stop_string.encode())
                self.idx = 0
                self.key = 0
                self.transitionTo(self.S0_WAITING)
                
    def transitionTo(self, newState):
        '''
        @brief transitions from current state to next
        @param newState The next desire state
        @return the new state to transition to
        '''
        self.state = newState
        return self.state
            