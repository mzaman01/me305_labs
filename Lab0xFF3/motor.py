'''
@file       motor.py
@brief      motor driver class that contains features of a DC motor
@details    This code contains a class that encapsulates all of the features associated with driving a DC motor connected to the DRV8847 motor driver that is part of thebase unit that you’ve connected your Nucleo to.  The base unit carries the DRV8847 motor driverfrom Texas Instruments which can be used to drive two DC motors or one stepper motor. 
            The link to the code can be found here: 
@author     Maisha Zaman
@date       March 18th, 2021
'''

from pyb import Timer, Pin

class MotorDriver:
    """ @brief This class implements a motor driver for the ME 305 Nucleo board"""
    
    def __init__(self, nSLEEP_pin, IN1_pin, IN2_pin, timer):
        ''' 
        @details Creates a motor driver by initializing GPIO pins and turning the motor off for safety.
        @param nSLEEP_pin   A pyb.Pin object to use as the enable pin.
        @param IN1_pin      A pyb.Pin object to use as the input to half bridge 1.
        @param IN2_pin      A pyb.Pin object to use as the input to half bridge 2.
        @param timer        A pyb.Timer object to use for PWM generation on 17IN1_pin and IN2_pin.
        '''
        ## A pyb.Pin object to use as the enable pin.
        self.nSLEEP_pin = nSLEEP_pin
        
        ## A pyb.Pin object to use as the input to half bridge 1.
        self.IN1_pin = IN1_pin
        
        ## A pyb.Pin object to use as the input to half bridge 2.
        self.IN2_pin = IN2_pin
        
        ## A pyb.Timer object to use for PWM generation on 17IN1_pin and IN2_pin.
        self.timer = timer
        
        ## timer object with timer object
        self.TIM = Timer(self.timer, freq = 20000)
        
        ## Connect Motor's pin 1 with timer channel
        self.IN1 = self.TIM.channel(1, mode=Timer.PWM, pin=self.IN1_pin)
        
        ## Connect Motor's pin 2 with timer channel
        self.IN2 = self.TIM.channel(2, mode=Timer.PWM, pin=self.IN2_pin)
        
        ## object to power the pins on or off
        self.EN = Pin(self.nSLEEP_pin, mode=Pin.OUT_PP, value=1)
    
    def enable(self):
       ''' 
       @brief activates the power flow by turning on the nSleep_pin
       '''
       self.EN.high()
    
    def disable(self):
        ''' 
        @brief deactivates the power flow by turning off the nSleep_pin
        '''
        self.EN.low()
    
    def set_duty(self, duty):
        '''
        @details This method sets the duty cycle to be sent to the motor to the given level. Positive values cause       
        effort in one direction, negative values in the opposite direction.
        @param duty signed integer holding the duty cycle of the PWM signal sent to the motor
        '''
        # duty is postive, pin 1 recieves duty, pin 2 does not
        if duty > 0 :
            self.IN1.pulse_width_percent(duty)
            self.IN2.pulse_width_percent(0)
        # duty is negative, pin 2 recieves duty, pin 1 does not
        elif duty < 0:
            self.IN1.pulse_width_percent(0)
            self.IN2.pulse_width_percent(abs(duty))
        # duty is equal to zero
        else:
            self.IN1.pulse_width_percent(0)
            self.IN2.pulse_width_percent(0)
    
if __name__ == '__main__':
    # Adjust the following code to write a test program for your motor class. Any code within the if __name__ == '__main__' block will only run when the script is executed as a standalone program. If the script is imported as a module the code block will not run.
    from encoder import Encoder
    encoder1 = Encoder(4, Pin.cpu.B6, Pin.cpu.B7, 0xFFFF, 0)
    
    print ("Testing Motor")
    # Create the pin objects used for interfacing with the motor driver
    pin_nSLEEP  = Pin.cpu.A15
    pin_IN1     = Pin.cpu.B4
    pin_IN2     = Pin.cpu.B5
    
    # Create the timer object used for PWM generation
    tim     = 3
    
    # Create a motor object passing in the pins and timer
    moe     = MotorDriver(pin_nSLEEP, pin_IN1, pin_IN2, tim)
    
    # Enable the motor driver
    moe.enable()
    
    # Set the duty cycle to 30 percent
    moe.set_duty(30)
    
    while True:
        try:
           encoder1.update()
           print("motor 1 position: ", encoder1.get_position())
           
        except KeyboardInterrupt:
           # This except block catches "Ctrl-C" from the keyboard to end the
           # while(True) loop when desired
           print('Ctrl-c has been pressed')
           # disable motor driver
           moe.disable()
           break
       
    
    