'''
@file       closedLoop.py
@brief      Closed Loop Controller class that controls speed of motor
@details    This code contains a class that helps incorporate the Encoder driver with the motor driver to perform closed-loop speed control. It takes in initial proportional gain for the system and provides saturation limits on the PWM level. 
            The link to the code can be found here: 
@author     Maisha Zaman
@date       March 18th, 2021
'''

class closedLoop:
     """ @brief This class implements a closed loop controller driver for the ME 305 Nucleo board"""
     
     def __init__(self):
        ''' 
        @details Creates a closed loop controller driver by initializing proportional gains, velocities and Motor effort
        '''
        ## Proportional controller gain
        self.Kp = 0
        
        ## Voltage applied to motor
        self.Vm = None
        
        ## PWM level
        self.L = 0
        
        ## Reference or Desired Velocity
        self.omegaRef = 0
        
        ## actual velocity of output
        self.omegaAct = 0
        
        ## New Proportional gain
        self.Kprime = 0
        
        def update(self):
            ''' 
            @brief Calculates PWM level needed to reached desire velocity
            '''
            self.Kprime = self.Kp/self.Vm
            self.L = self.Kprime*(self.omegaRef - self.omegaAct)
            
            # correct for PWM saturation
            if self.L >= 100:
                self.L = 100
            elif self.L <= 100:
                self.L = -100
            else:
                pass
        
        def get_Kp(self):
            ''' 
            @brief retrieves the proportional controller gain value
            @return the proportional controller gain value
            '''
            return self.Kp
        
        def set_Kp(self, new_Kp):
            ''' 
            @brief Sets the proportional controller gain value
            @param new_Kp new desired controller gain value
            '''
            ## new desired controller gain value
            self.new_Kp = new_Kp
            self.Kp = self.new_Kp
          
        