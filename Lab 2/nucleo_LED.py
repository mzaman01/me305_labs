'''
@file       nucleo_LED.py
@brief      Nucleo's LED transitions between 3 pulse wave cycles
@details    Implements a finite state machine, shown below, to simulate
            the behavior of a Nucleo's LED (LD2) transtioning between three pulse
            cycles (Square,Sine,Saw-Tooth) by clicking on the blue user button 
            on the Nucleo.
            The link to the code can be found here: https://bitbucket.org/mzaman01/me305_labs/src/master/Lab%202/nucleo_LED.py

@image html me305lab0x02.png "Nucleo LED Finite State Transition Diagram "  
@author     Maisha Zaman
@date       February 4th, 2021
'''

import pyb
import math
import utime

## setting global variable equal to 0
myVar = 0
## set start time to zero
start = 0

def myCallback(IRQ_source):
    """
    @brief interrupt callback called when blue user button is pushed 
    @param IRQ_src the interrupt request state
    """
    #modify variable outside of scope
    global myVar
    #add variable after each button press, will affect which state enters next
    myVar += 1
    
    #modify start outside of scope
    global start
    # record start time (ms) each time button is pushed 
    start = utime.ticks_ms()
    
    #limit so global variable cannot exceed 4
    if myVar > 4:
        myVar = 4
        

if __name__ == "__main__":
    
    ## Connecting to pinA5 to control LD2 LED
    pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
    
    ## create object using timer 2, trigger at 20000 hz (cycle/sec)
    tim2 = pyb.Timer(2, freq = 20000)
    
    ## Set up timer channel for LD2 LED
    t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)
    
    ## Define pin object for PC13 - for blue user button
    pinC13 = pyb.Pin (pyb.Pin.cpu.C13)
        
    ## Associate the callback function with the pin by setting up an external interrupt
    ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING,pull=pyb.Pin.PULL_NONE, callback=myCallback)
    
    ## Setting state to initial state
    state = 0
     
    #Print instructions of user
    print("Welcome to your nucleo board!\nPush the blue user button",
          "B1 on the Nucleo to cycle through the 3 LED Patterns.")
    
    while True:
        try:
            #Initialize program
            if state == 0:
                
                #begin the LED's square-wave pattern if button is pushed
                if myVar == 1:
                    print('Square-Wave Pattern Selected')
                    # print(myVar)
                    state = 1
                    
                # fix if button is pushed more than once accidentally
                elif myVar > 1:
                    myVar = 1
                        
            elif state == 1:
                
                ## start recording time for state 1
                start1 = utime.ticks_ms()
                ## find difference of time (ms->sec) between button click and state start
                duration = (utime.ticks_diff(start1,start)/1000)
                ## percentage of LED brightness
                duty = 100*(duration%1)
                # print(start/1000, start1/1000, duration, duty )
                
                if duty > 0 and duty <= 50:
                    t2ch1.pulse_width_percent((100))
                    
                else:  #assume it is 50-100
                    t2ch1.pulse_width_percent((0))

                #begin the LED's sine-wave pattern if button is pushed
                if myVar == 2:
                    print('Sine-Wave Pattern Selected')
                    state = 2
                    
                # fix if button is pushed more than once accidentally
                elif myVar > 2:
                    myVar = 1
            
            elif state == 2:
                
                ## start recording time for state 2
                start2 = utime.ticks_ms()
                # find difference of time (ms->sec) when button is clicked to now
                duration = (utime.ticks_diff(start2,start)/1000)
                #define sine function
                duty = (50*(math.sin(2*math.pi*duration/10)))+50
                t2ch1.pulse_width_percent((duty))
                
                # print(start/1000, start2/1000, duration, duty )
                
                # begin the LED's sawtooth-wave pattern if button is pushed
                if myVar == 3:
                    print('Saw-Tooth Pattern Selected')
                    state = 3
                elif myVar > 3:
                    myVar = 2
                
            
            elif state == 3:
                ## start recording time for state 3
                start3 = utime.ticks_ms()
                # find difference of time (ms->sec) when button is clicked to now
                duration = (utime.ticks_diff(start3,start)/1000)
                # start saw tooth wave
                duty = 100*(duration%1)
                t2ch1.pulse_width_percent((duty))
                
                # print(start/1000, start3/1000, duration, duty )

                
                #begin the LED's square-wave pattern if button is pushed
                if myVar == 4:
                    print('Square-Wave Pattern Selected')
                    myVar = 1
                    state = 1
                # fix if button is pushed more than once accidentally
                elif myVar > 4:
                    myVar = 3
                
        except KeyboardInterrupt:
            # This except block catches "Ctrl-C" from the keyboard to end the 
            # while(True) loop when desired
            
            # Clean up code, unbinds the interrupt
            pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING, pull=pyb.Pin.PULL_NONE, callback=None)
            print("Ctrl-C has been pressed, see you next time!")
            break

