'''
@file       elevator_fsm.py
@brief      Simulates a elevator moving between Floor 1 and Floor 2
@details    Implements a finite state machine, shown below, to simulate
            the behavior of an elevator transtioning between floor 1 and floor
            floor 2. A randomizer is used for button selection to indicate
            whether the elevator will go to a different floor or stay put. The
            link to the code can be found here: https://bitbucket.org/mzaman01/me305_labs/src/master/hw%202/elevator_fsm.py

@image html me305hw0x00maishazaman.png "Elevator Finite State Transition Diagram "  
@author     Maisha Zaman
@date       January 27, 2021
'''
import time
import random

def motor_cmd(cmd):
    """
    @brief Commands the motor to move up, down or stop
    @param cmd the command to give the motor
    """

    if cmd == 0:
        print("Motor Stop")
    elif cmd == 1:
        print("Motor Upwards")
    elif cmd == 2:
        print("Motor Downwards")
        
def button1_cmd():
    """
    @brief Commands the button 1 to be pushed
    @return returns a True or False value to indicate if button 1 is pushed
    """
    return random.choice([True,False]) #randomly returns T or F, bc we don't
                                        #have an actual button so it randomizes

def button2_cmd():
    """
    @brief Commands the button 2 to be pushed
    @return returns a True or False value to indicate if button 2 is pushed
    """
    return random.choice([True,False]) #randomly returns T or F, bc we don't
                                        #have an actual button so it randomizes
def floor1_sensor():
    """
    @brief Senses if elevator is on floor 1
    @return returns a True or False value to indicate if sensor 1 is triggered
    """
    return random.choice([True,False]) #randomly returns T or F, bc we don't
                                        #have an actual button so it randomizes

def floor2_sensor():
    """
    @brief Senses if elevator is on floor 2
    @return returns a True or False value to indicate if sensor 2 is triggered
    """
    return random.choice([True,False]) #randomly returns T or F, bc we don't
                                        #have an actual button so it randomizes

# Main program/ test program begin
# This code only runs if the script is executed as main by pressing play but 
# does not run if the script is imported as a module
if __name__ == "__main__":
    
    # Program initilization goes here
    ## initial state is the init state
    state = 0 
    
    while True:
        try:
            
            #Main program code goes here
            if state == 0:  
                # run state 0 (moving down) code
                print('S0, Moving Down')
                #if floor 1 sensor is triggered, stop motor and transition S1
                if floor1_sensor():
                    motor_cmd(0)    #command motor to stop
                    state = 1       #Update state for next interation
                
            elif state == 1:
                #run state 1(stopped on Floor 1) code
                print('S1, Stopped on Floor 1')
                #if button 1 pushed, provide error message and stay in S1
                if button1_cmd():
                    print("Button 1 is Pushed")
                    print("You're already on Floor 1")
                    state = 1
                #if button 2 pushed, motor moves up and transitions to state 2
                elif button2_cmd():
                    print("Button 2 is Pushed")
                    motor_cmd(1)
                    state = 2
                
            elif state == 2:
                #run state 2(moving up) code
                print('S2, Moving Up')
                #if floor 2 sensor is triggered, stop motor and transition S3
                if floor2_sensor():
                    motor_cmd(0)
                    state = 3
            
            elif state ==3 :
                #run state 3(stopped on Floor 2) code
                print('S3, Stopped on Floor 2')
                 #if button 2 pushed, provide error message and stay in S3
                if button2_cmd():
                    print("Button 2 is Pushed")
                    print("You're already on Floor 2")
                    state = 3
                #if button 1 pushed, motor moves up and transitions to state 2
                elif button1_cmd():
                    print("Button 1 is Pushed")
                    motor_cmd(2)
                    state = 0
                
            else:
                #code to run if state number is invalid
                # program should ideally never reach here
                pass
            
            # slows down loop, will run 5 times every second
            time.sleep(0.2)
            
        except KeyboardInterrupt:
            # This except block catches "Ctrl-C" from the keyboard to end the 
            # while(True) loop when desired
            break
           
                
            