'''
@file       simonSays_main.py
@brief      A Simon Says Game implementation on the Nucleo L476
@details    Implements a class to simulate a game of 'Simon Says' using morse 
            code and the LED (LD1) on the Nucleo L476.
            The link to the code can be found here: https://bitbucket.org/mzaman01/me305_labs/src/master/Lab%203/simonSays_main.py
            The link to the video can be found here: https://youtu.be/jU0XBBrgE_I
@author     Maisha Zaman
@date       February 18th, 2021 
'''
import simonTask 
import pyb

# Main program / test program begin
#   This code only runs if the script is executed as main by pressing play
#   but does not run if the script is imported as a a module
if __name__ == "__main__":
    
    ## Connecting to pinA5 to control LD2 LED
    pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
    
    ## create object using timer 2, trigger at 20000 hz (cycle/sec)
    tim2 = pyb.Timer(2, freq = 20000)
    
    ## Set up timer channel for LD2 LED
    t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)
    
    ## Define pin object for PC13 - for blue user button
    pinC13 = pyb.Pin (pyb.Pin.cpu.C13)
    
    # Program initialization goes here
    
    ## Assigning Simon Says Game to task
    task1 = simonTask.simonTask(DBG_flag = False)
    
    while True:
        try:
            task1.run()
            
        except KeyboardInterrupt:
            # This except block catches "Ctrl-C" from the keyboard to end the
            # while(True) loop when desired
             # Clean up code, unbinds the interrupt
            pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING, pull=pyb.Pin.PULL_NONE, callback=None)
            print('Ctrl-c has been pressed')
            
            break

    # Program de-initialization goes  here
    