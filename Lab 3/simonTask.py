'''
@file       simonTask.py
@brief      A class that will run the Simon Says Game on the Nucleo
@details    Implements a finite state machine, shown below, to simulate
            a game of 'Simon Says' using morse code and the LED (LD1) on the
            Nucleo L476. The game gets longer as the rounds increase. The code 
            measures when the user turns on and off the LED with the push and
            release of the button. The pattern is randomly generated between turning
            on and off between either 0.5 sec or 1 sec. There is a tolerance of 0.3 seconds for 
            the user push. If the user fails to replicate the pattern, the round
            will end and the user will be given a choice to continue or end
            the game. When the user quits the game the total score is shown.
            The link to the code can be found here: https://bitbucket.org/mzaman01/me305_labs/src/master/Lab%203/simonTask.py
            The link to the video can be found here: https://youtu.be/jU0XBBrgE_I
@image html me305lab0x03.png "Simon Says Finite State Transition Diagram "  
@author     Maisha Zaman
@date       February 18th, 2021
'''
import random
import pyb
import utime
import sys
import micropython
micropython.alloc_emergency_exception_buf(200)




class simonTask:
    # Static variables are defined outside of methods
    
    ## Initiliaze Program with State 0
    S0_GENERATE_PATTERN             = 0
    ## State 1 initialized the pattern shown to the user on the LED
    S1_START_PATTERN                = 1
    ## State 2 will recieve the pattern inputted by user
    S2_USER_PATTERN                 = 2
    ## State 3 will provide results whether the user won the game or not
    S3_END_ROUND                    = 3
    
    ## tolerance of user pushing button in seconds
    tolerance = 0.35
    
    def __init__(self, DBG_flag=True):
        # This not your state 0
        
        ## The current state of the finite state machine
        self.state = 0
        
        ## Counts the number of runs of our task
        self.round = 1
        
        ## Number of Wins
        self.wins = 0
        
        ## Number of Losses
        self.losses = 0 

        ## Global State of button not being pressed
        self.press = 0
                
        ## Global Button initialized as not being pushed
        self.button_press = False
        
        ## record start time (ms) each time button is pushed 
        self.buttonStart = 0
        
        ## Record end time (ms) when button is released
        self.buttonEnd = 0
        
        ## Flag to specify if debug messages print
        self.DBG_flag = DBG_flag
        
         ## the time choices (sec) when the led will be on
        self.ledOn = [0.5,1]
        
        ## the time choices(sec) when the led will be off
        self.ledOff = [0.5,1]
        
        ## number of items in sequence when LED is on
        self.modeOn = 2
        
        ## number of pauses in sequence when LED if off
        self.modeOff = self.modeOn - 1
        
        ## indexing value for the list of LED times
        self.i = 0
        
        ## Game set to start when it enters State 1
        self.game = True
        
        ## Connect to pinA5 to control LD2 LED
        self.pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
        
        ## create object using timer 2, trigger at 20000 hz (cycle/sec)
        self.tim2 = pyb.Timer(2, freq = 20000)
        
        #Set up timer channel for LD2 LED
        self.t2ch1 = self.tim2.channel(1, pyb.Timer.PWM, pin=self.pinA5)
        
        ## Define pin object for PC13 - for blue user button
        self.pinC13 = pyb.Pin (pyb.Pin.cpu.C13)
        
        ## Associate the callback function with the pin by setting up an external interrupt
        self.ButtonInt = pyb.ExtInt(self.pinC13, mode=pyb.ExtInt.IRQ_RISING_FALLING,pull=pyb.Pin.PULL_NONE, callback=self.button_pressed)
        
        ## start time recording
        self.start = utime.ticks_ms()
        
        print("""Welcome to the Simon Says Game! \nA certain pattern will light up on the LED in intervals of 0.5 or 1 seconds. \nRepeat the pattern by holding onto the blue button. \nGet the timing correct when LED is both OFF and ON to recieve points. \nIf you make a mistake the round will end. \nTo win a round you must complete the entire pattern correctly. \nAs the rounds progress, the patterns will get longer. \nPress the button to begin the game and good luck!""")
    
        print("\nRound 1 will begin in 3 seconds")
     
    def run(self):
        '''
        @brief runs the finite state machine of the Simon Says Game
        '''

         
         
        if self.DBG_flag:
            print('State' + str(self.state) + ": Round" + str(self.round))
        
        # main program code goes here
        if self.state==self.S0_GENERATE_PATTERN:
                   
            #reset iteration value
            self.i = 0
        
            ## randomize list when LED is on
            self.onList = self.randChoices(self.ledOn,self.modeOn)
            ## randomize list when LED is off
            self.offList = self.randChoices(self.ledOff,self.modeOff)
                    
            # print("onList: ", self.onList, "offList: ", self.offList)
        
            # Press button to start the round
         
            ## time passing after button is released
            self.now = utime.ticks_ms()
            
            ## time difference between start of code and current state
            self.duration = utime.ticks_diff(self.now, self.start)/1000
            # print("ROUND: ", self.round , "duration: ", self.duration )
            
            if self.duration == 1:
                print("Starting in: 3")
            if self.duration == 2:
                print("Starting in: 2")
            if self.duration >= 3:
                print("Starting in: 1")
                # when 3 seconds has passed, start LED pattern
                self.transitionTo(self.S1_START_PATTERN)   # Updating state for next iteration 
            
            # # Press button to start the round
            # if self.button_press:
            #     # time passing after button is released
            #     self.start = utime.ticks_ms()
            #     self.duration = utime.ticks_diff(self.start, buttonEnd)/1000
            #     print("ROUND: ", self.round , "\nThe Pattern will display in 0.5 seconds")
                
            #     if self.duration == 0.5:
            #         # when 0.5 seconds has passed, start LED pattern
            #         self.transitionTo(self.S1_START_PATTERN)   # Updating state for next iteration            
      
            
        elif self.state==self.S1_START_PATTERN:
            # run state 1 (starting the pattern) code
            # print("onList: ", self.onList, "offList: ", self.offList)
            self.game = True
            # print("onList: ", self.onList, "offList: ", self.offList)
            while self.game:
                
                # end game when LED on list is finished iterating
                if self.i > self.modeOn-1:
                    print("\nLED pattern finished, Repeat the Pattern")
                    #reset i back to 0 
                    self.i = 0
                    # When pattern is finished playing on LED, transition to S2
                    self.transitionTo(self.S2_USER_PATTERN)
                    break
                
                # if LED off list finished iterating, only set LED on list
                elif self.i> self.modeOff-1:
                    ## set the on time for this round
                    self.onTime = self.onList[self.i]
                    
                # if list is iterable, define time for when LED is on and off
                else:
                    ## set the on time for this round
                    self.onTime = self.onList[self.i]
                    ## set the off time for this round
                    self.offTime = self.offList[self.i]
                
                
                # turn LED on to 100%
                self.t2ch1.pulse_width_percent(100)
                ## record time when pin is on
                self.start = utime.ticks_ms()
                
                while True:
                    ## current time is updating constantly for LED on
                    self.curr_time = utime.ticks_ms()
                    self.duration = utime.ticks_diff(self.curr_time,self.start)/1000
                    # print( "ROUND:", round, "onTime: ", self.onTime, 'duration: ', self.duration, "i: ", self.i)
                    
                    # when duration is equal or greater than LED time, LED off
                    if self.duration >= self.onTime:
                        self.t2ch1.pulse_width_percent(0)
                        # print("LED TURNED OFF")
                        break
                     
                if self.i <= self.modeOff - 1:   
                    # record time when pin is off
                    self.start = utime.ticks_ms()
                    
                    while True:
                        # current time is updating constantly
                        self.curr_time = utime.ticks_ms()
                        self.duration = utime.ticks_diff(self.curr_time,self.start)/1000
                        # print( "ROUND:", round, "offTime: ", self.offTime, 'duration: ', self.duration, "i: ", self.i)
        
                        # when duration is equal or greater than LED time, LED on
                        if self.duration >= self.offTime:
                            # print("LED TURNED ON")
                            break  
                self.i += 1
            
            
        elif self.state==self.S2_USER_PATTERN:
            # run state 2 (recieving pattern data from user) code
            self.game = True
            self.start = utime.ticks_ms()
            while self.game:
            
                if self.i> self.modeOn-1: 
                    print("You won the round, congratulations!")
                    self.t2ch1.pulse_width_percent(0)
                    self.game = False
                    self.wins += 1
                    # if pattern passed, transition to S3
                    self.transitionTo(self.S3_END_ROUND)
                    break 
                                  
                elif self.i>self.modeOff-1:
                    ## set the on time for this round
                    self.onTime = self.onList[self.i]
                
                else:
                    ## set the on time for this round
                    self.onTime = self.onList[self.i]
                    ## set the off time for this round
                    self.offTime = self.offList[self.i]
                
                # wait until button is pushed, LED off
                while not self.button_press:
                    self.now = utime.ticks_ms()
                    ## time difference of button not being pressed
                    self.notPress = utime.ticks_diff(self.now,self.start)/1000
                    # print("button not pressed", self.notPress)
                    self.t2ch1.pulse_width_percent(0)
                     # end round if user takes too long to start game
                if self.notPress >= 10:
                    print("Took too long to press the button. You've lost this round.")
                    self.game = False
                    self.losses += 1
                    # if patter passed, transition to S3
                    self.transitionTo(self.S3_END_ROUND)
                    break
                else:
                    pass
        
                #wait for button release
                while self.button_press:
                    self.t2ch1.pulse_width_percent(100)
                    pass
                
                ## record time when button is pushed 
                self.releaseTime = utime.ticks_ms()
                ## time difference between release time and button push
                self.pressDuration = utime.ticks_diff(self.releaseTime,self.buttonStart)/1000
                # print("button pressed Duration: ", self.pressDuration, "time number: ", self.i)
                
                if self.pressDuration >= self.onTime-self.tolerance and self.pressDuration <= self.onTime+self.tolerance:
                    # print("congrats!!!")
                    pass
                
                # end round if it doesn't match
                elif self.pressDuration < self.onTime:
                    print("Pushed too Fast! You've lost this round. Better luck next time")
                    self.t2ch1.pulse_width_percent(0)
                    self.game = False
                    self.losses += 1
                    # if pattern failed, transition to S3
                    self.transitionTo(self.S3_END_ROUND)
                    break
                
                # end round if it doesn't match
                elif self.pressDuration > self.onTime:
                    print("Pushed too slow! You've lost this round. Better luck next time")
                    self.t2ch1.pulse_width_percent(0)
                    self.game = False
                    self.losses += 1
                    # if patter failed, transition to S3
                    self.transitionTo(self.S3_END_ROUND)
                    break
                
                if self.i <= self.modeOff - 1:
                    # record time for LED off, wait until button is pushed
                    while not self.button_press:
                        self.t2ch1.pulse_width_percent(0)
                        pass
                    
                    
                    # record when button is release and now
                    # print("offTime Mode")
                    ## time recording starts when led is off
                    self.offCurr = utime.ticks_ms()
                    ## time duration when LED is off
                    self.offDuration = utime.ticks_diff(self.offCurr,self.buttonEnd)/1000
                    # print("button not pushed Duration: ", self.offDuration, "time number: ", self.i)
                    
                   
                    # pass if it equals the time of LED OFF
                    if self.offDuration >= self.offTime-self.tolerance and self.offDuration <= self.offTime+self.tolerance:
                        # print("congrats on not pushing!")
                        pass
                              
                    # end round it doesn't match
                    elif self.offDuration < self.offTime:
                        print("Released too fast! You've lost this round. Better luck next time")
                        self.game = False
                        self.losses += 1
                        # if pattern failed, transition to S3
                        self.transitionTo(self.S3_END_ROUND)
                        break      
                      # end round it doesn't match
                    elif self.offDuration > self.offTime:
                        print("Released too slow! You've lost this round. Better luck next time")
                        self.game = False
                        self.losses += 1
                        # if pattern failed, transition to S3
                        self.transitionTo(self.S3_END_ROUND)
                        break   
          
                self.i+=1
             
            
        elif self.state==self.S3_END_ROUND:
            # run state 3 (end round) code
            self.t2ch1.pulse_width_percent(0)
            # print Round is over message
            print("\nRound: ", self.round , "has ended.")
            
            ## User input to continue or quit game
            self.Choice = input("To continue playing, Type 'play'. To quit game, type 'quit': ")
            
            # continue game if user types "yes"
            if self.Choice[0] == 'p' or self.Choice[0] == "P":
                # run state 0 (generate pattern) code, allow for user to press button to begin
                self.round += 1
                
                # increase the pattern length by +1 after each round
                self.modeOn += 1
                self.modeOff += 1
                
                ## start time recording
                self.start = utime.ticks_ms()
                
                #Start over FSM
                self.transitionTo(self.S0_GENERATE_PATTERN)
            
            # quit and display results
            elif self.Choice[0] == 'q' or self.Choice[0] == "Q":
                print("\nTotal Wins: ", self.wins , "Total Losses: ", self.losses)
                print("\nThank you for playing! See you Next Time.")
                #Clean up code, unbinds the interrupt
                pyb.ExtInt(self.pinC13, mode=pyb.ExtInt.IRQ_RISING_FALLING, pull=pyb.Pin.PULL_NONE, callback=None)
                sys.exit(0)
                
            # else, repeat question:
            else:
                print("Please Try Again.\n")
                
            

        else:
            pass
            # code to run if state number is invalid
            # program should ideally never reach here
                
    def transitionTo(self, newState):
        '''
        @brief transitions from current state to next
        @param newState The next desire state
        @return the new state to transition to
        '''

        if self.DBG_flag:
            print(str(self.state) + "->" + str(newState))
        self.state = newState
        return self.state
    
    def button_pressed(self,IRQ_source):
        """
        @brief interrupt callback called when blue user button is pushed  down
        @param IRQ_src the interrupt request state
        @return if button is pressed or not pressed
        """
    
        #modify start outside of scope
        # global buttonStart
        # global buttonEnd
        # global press
        # global button_press # allows callback to recognized outside of function
        
        self.button_press = True
        self.press += 1
        if self.press == 1:
            # record start time (ms) each time button is pushed 
            self.buttonStart = utime.ticks_ms()
        else:
            self.press = 0
            #record when button is released
            self.buttonEnd = utime.ticks_ms()
            self.button_press = False
            
        return self.button_press
    
    
    def randChoices(self,List,k):
        """
        @brief returns multiple randomized values from list
        @details Imitates random.choices() that is not available in micropython. 
        @param List list of numbers or strings to be randomly selected
        @param k number of items to outputted
        @return return list of randomized items
        """
        ## empty list to append new items into
        self.randList = []
        ## inputted list of time items for LED to turn on and off
        self.List = List
        ## length of list
        self.k = k
        
        # Loop through randomization based off the inputted cycles
        for self.num in range(0,self.k):
            ## randomly select item location
            self.randNum = random.randrange(0,len(self.List))
            # add item into list
            self.randList.append(self.List[self.randNum])
            
        return self.randList

    
    
    