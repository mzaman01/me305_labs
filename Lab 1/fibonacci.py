"""
@file       fibonacci.py
@brief      Calculates the Fibonacci number of a inputted real positive number 
@details    Implements a Bottom Up Approach to calculate the desired Fibonacci number after the user has inputted a real, positive number
@author     Maisha Zaman
@date       January 21, 2021
"""

def fib(idx):
    ''' 
    @brief   This function calculates a Fibonacci number at a specific index
    @param idx An integer specifying the index of the desired Fibonacci number
    @return The Fibonacci number at the specified index
    '''
    
    if idx == 0:
        return 0
    elif idx == 1 or idx == 2:
        return 1
    else:
        num1 = 0
        num2 = 1
        for number in range(idx):           
            fibnum = num1 + num2
            num2 = num1
            num1 = fibnum
        return fibnum

if __name__ == '__main__':
    
    #Code Introduction and Exit Statement
    print("Thank you for using this Fibonacci number Calculator. Type 'quit' in the input section to exit program. ")
  
    
    # Do not allow code to run until user has inputted a valid index
    run = True
    while run:   
        try: 
            #Ask user to input a number to calculate
            idx = (input("Input a positive real index for a desired Fibonacci number: "))
            
            # if the string contains a positive real number, calculate
            if idx.isdigit():
                # convert string into integer
                idx = int(idx)
                #call fib(idx) function and print results
                print('Fibonacci number at index {:} is {:}'.format(idx, fib(idx)))
                
            # Exit program when 'quit' is entered
            elif idx == "quit": 
                print("Thank you for using the calculator, see you next time!")
                #exit while loop
                run = False
                
            else:
                #if non-real number is inputted repeat loop
                print("That is not a positive real integer, please try again ")
                
        except KeyboardInterrupt:
            # This except block catches "Ctrl-C" from the keyboard to end the 
            # while loop when desired    
            run = False
            print("\nThank you for using the calculator, see you next time!")
        
       
        
    
    
