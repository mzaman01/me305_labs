'''
@file       encoder.py
@brief      Class of time read from an encoder connected to arbitrary pins
@details    This class contains functions that update the motor's counter through
            pins conncted to encoder wires. The class corrects for any overflow
            of the timer and records delta positions and saves the previous position. 
            The link to the code can be found here: https://bitbucket.org/mzaman01/me305_labs/src/master/Lab0xFF2/encoder.py
@author     Maisha Zaman
@date       March 18th, 2021
'''

from pyb import Timer, Pin

class Encoder:
     
    
    def __init__(self,timerNum,pin1,pin2, Period, Prescaler):
        """
        @brief An encoder driver that reads timer outputs from connected pins
        @details The driver takes the timer channels, 2 pin numbers, period and
                prescaler that will be utilized to count an encoder channel. The
                driver accounts for timer overflow and is used to control a 
                motor's position. 
        @param timerNum the timer channel for the set of pins
        @param pin1 the first pin the encoder is connected to
        @param pin2 the second pin the encoder is connected to
        @param Period the largest number that can stored in timer
        @param Prescaler the frequency dividier or period multiplier
        """
        
        ## Timer for the set of encoder pins
        self.timerNum = timerNum  
        
        ## first pin for Encoder
        self.pin1 = pin1
        
        ## second pin for Encoder
        self.pin2 = pin2
        
        ## period for running the timer object
        self.Period = Period
        
        ## prescaler for the timer object
        self.Prescaler = Prescaler
        
        ## Timer object for Encoder
        self.TIM = Timer(self.timerNum, period=self.Period, prescaler=self.Prescaler)
        
        ## Channel 1 for Timer object
        self.TIM.channel(1, mode=Timer.ENC_AB, pin=self.pin1)
        
        ## Channel 2 for Timer object
        self.TIM.channel(2, mode=Timer.ENC_AB, pin=self.pin2)
        
        ## Current position
        self.currPosition = 0
        
        ## Previous position
        self.prevPosition = 0
      
    def update(self):
        """
        @brief updates the recorded position of the encoder
        @details This update counts the position of the motor, and fixes the 
                delta readings by adjusting depending on what the defined period
                is. This function stores the previous counter value and creates
                a new counter value each time the function is ran.
        """
    
        #update current position to counter
        self.currPosition = self.TIM.counter()
        
        ## difference between last two recorded positions
        self.delta = self.currPosition - self.prevPosition
  
        # fixing delta readings and adjusting position
        if self.delta > 0.5*self.Period:
            self.delta -= self.Period
        elif self.delta < -0.5*self.Period:
            self.delta += self.Period
        else:
            pass
        
        #store previous position
        self.prevPosition = self.currPosition
        
        # add corrected delta to current position
        self.currPosition += self.delta
     
        
    def get_position(self):
        """
        @brief returns the most recently updated position of the encoder
        @return the most recent updated position value
        """
        #return latest value
        return self.currPosition
    
    def set_position(self, newPosition):
        """
        @brief resets the position to a specified value
        @details this function takes in a new position value and replaces it 
                with the current value. The previous current value is stored in
                a previous position variable.
        @param newPosition a new position set by the user
        @return new set current position
        """
        ## new position 
        self.newPosition = newPosition
        
        #store previous position
        self.prevPosition = self.currPosition
        
        #update current position to counter
        self.currPosition = self.newPosition
        
        return self.currPosition
        
    
    def get_delta(self):
        """
        @brief returns difference between the last two recorded position
        @return the difference of the last two updated positions
        """
        ## difference between last two recorded positions
        self.delta = self.currPosition - self.prevPosition
        
        return self.delta
    
