'''
@file       task1.py
@brief      controller that updates the encoder driver at a regular interval
@details    This code contains a class that runs the encoder driver and stores 
            values in shares.py. It updates encoder object at the requisite interval
            defined by the user and allows for other commands such as storing
            the delta, position and setting a value to the position.
            The link to the code can be found here: https://bitbucket.org/mzaman01/me305_labs/src/master/Lab0xFF2/task1.py
@image html me305lab0xFF4.png "Finite State Transition Diagram of Encoder Controller" 
@author     Maisha Zaman
@date       March 18th, 2021
'''

from encoder import Encoder
import shares
import utime
import micropython
from pyb import Pin
micropython.alloc_emergency_exception_buf(200)

class controllerTask:
    # Static variables are defined outside of methods
    
    ## Initiliaze Program with State 0, updates encoder positions
    S0_UPDATE         = 0
    ## State 1 starts collecting and storing data
    S1_COLLECT_DATA     = 1

    
    def __init__(self, encPeriod):
        """
        @brief Class that runs the encoder driver
        @details runs the encoder driver and stores values in shares.py.
        @param encPeriod user defined period to update encoder positions
        """
        ## first encoder object channel 
        self.encoder1 = Encoder(4, Pin.cpu.B6, Pin.cpu.B7, 0xFFFF, 0)
        ## second encoder object channel
        self.encoder2 = Encoder(8, Pin.cpu.C6, Pin.cpu.C7, 0xFFFF, 0)
        
        ## period for encoder to update for
        self.encPeriod = encPeriod
        
        ## TimeStamp for last iteration of the task in microseconds
        self.lastTime = utime.ticks_us()
        
        ## timestamp for the next iteration of the task in microseconds
        self.nextTime = utime.ticks_add(self.lastTime, self.encPeriod)

        ## initialize state
        self.state = 0
        
         
    def run(self):
        """
        @brief updates encoder object at the requisite interval
        """
        ## time when class first starts running in microseconds
        self.thisTime = utime.ticks_us()
        
        if utime.ticks_diff(self.thisTime,self.nextTime) >= 0:
            
            ## timestamp for the next iteration of the task
            self.nextTime = utime.ticks_add(self.lastTime, self.encPeriod)
            
        
            if self.state == self.S0_UPDATE:
                # print("Task 1 state 0")
                # update position 
                self.encoder1.update()
                self.encoder2.update()    
                
                self.transitionTo(self.S1_COLLECT_DATA)
                
                
            elif self.state == self.S1_COLLECT_DATA:
                # print("Task 1 state 1")
                ## update and set position to encoder 1
                shares.enc1Pos = self.encoder1.get_position()
                ## update and set position to encoder 2
                shares.enc2Pos = self.encoder2.get_position() 
                
                # print('Encoder 1 position: ', shares.enc1Pos,'Encoder 2 position: ', shares.enc2Pos )
                
                ## last two recorded position difference for encoder 1
                shares.enc1Del = self.encoder1.get_delta()
                ## last two recorded position difference for encoder 2
                shares.enc2Del = self.encoder2.get_delta()
                
                # print('Encoder 1 delta: ', shares.enc1Del,'Encoder 2 delta: ', shares.enc2Del )
                
                # set position to 0
                if shares.setZero1:
                    shares.enc1Pos = self.encoder1.set_position(0)    
                    shares.setZero1 = False
                                    
                elif shares.setZero2:
                    shares.enc2Pos = self.encoder2.set_position(0)    
                    shares.setZero2 = False
                else:
                    pass
                
                self.transitionTo(self.S0_UPDATE)
                    
                
    def transitionTo(self, newState):
        '''
        @brief transitions from current state to next
        @param newState The next desire state
        @return the new state to transition to
        '''
        self.state = newState
        return self.state
            