'''
@file       shares.py
@brief      Creates and defines passive varables to be shared between files
@details    This code defines and initiates variable objects
            that is shared between UI_Task and the controller task. 
            The link to the code can be found here: https://bitbucket.org/mzaman01/me305_labs/src/master/Lab0xFF2/shares.py
@author     Maisha Zaman
@date       March 18th, 2021
'''

## encoder 1 's position
enc1Pos = 0
## encoder 2's postion
enc2Pos = 0
## encoder 1's delta
enc1Del = 0
## encoder 2's delta
enc2Del = 0
## Trigger to set encoder 1's position to zero
setZero1 = False
## Trigger to set encoder 2's position to zero
setZero2 = False